import React from "react";
import BarChart from "./bar-chart";

import * as COLORS from "../../constants/colors";
import { AnimationType } from "../../constants/enums";
import { useInterval } from "../session/use-interval";

const Visualizer = ({ animations, array, setArray, delay }) => {
  useInterval(
    () => {
      const { bars, height, type } = animations.shift();

      const newArray = [...array];
      switch (type) {
        case AnimationType.COMPARE:
          bars.map(bar => (newArray[bar].color = COLORS.SECONDARY));
          break;
        case AnimationType.DECOMPARE:
          bars.map(bar => (newArray[bar].color = COLORS.PRIMARY));
          break;
        case AnimationType.RESIZE:
          bars.map(bar => (newArray[bar].height = height));
          break;
        default:
      }
      setArray(newArray);
    },
    animations.length > 0 ? delay : null
  );

  return (
    <>
      <main id="bars">
        <BarChart bars={array} />
      </main>
    </>
  );
};

export default Visualizer;
