import React from "react";

const Bar = ({ id, color, height }) => {
  return (
    <div
      className="bar"
      key={id}
      style={{
        backgroundColor: color,
        height: `${height}px`
      }}
    ></div>
  );
};

export default Bar;
