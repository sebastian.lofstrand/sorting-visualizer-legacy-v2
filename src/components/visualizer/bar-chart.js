import React from "react";
import Bar from "./bar";

const BarChart = ({ bars }) => {
  return (
    <div className="bar-chart">
      {bars.map(bar => {
        const { id, color, height } = bar;
        return <Bar key={id} id={id} color={color} height={height} />;
      })}
    </div>
  );
};

export default BarChart;
