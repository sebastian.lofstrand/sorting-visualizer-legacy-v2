import { getInsertionSortAnimations } from "./insertion-sort";
import { getMergeSortAnimations } from "./merge-sort";
import { getShellSortAnimations } from "./shell-sort";
import { getSelectionSortAnimations } from "./selection-sort";
import { getQuickSortAnimations } from "./quick-sort";

export {
  getInsertionSortAnimations,
  getMergeSortAnimations,
  getSelectionSortAnimations,
  getShellSortAnimations,
  getQuickSortAnimations
};
