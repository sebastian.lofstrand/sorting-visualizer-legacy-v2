import { AnimationType } from "../../constants/enums";
import { cloneArray } from "../../utils";

export function getSelectionSortAnimations(array) {
  const animations = [];
  if (array.length <= 1) return array;

  selectionSort(cloneArray(array), animations);
  return animations;
}

let selectionSort = (array, animations) => {
  let len = array.length;
  for (let i = 0; i < len; i++) {
    let compare, min;
    compare = min = i;

    animations.push({
      bars: [compare],
      type: AnimationType.COMPARE
    });
    for (let j = i + 1; j < len; j++) {
      animations.push({
        bars: [j],
        type: AnimationType.COMPARE
      });

      if (array[min].height > array[j].height) {
        min = j;
      }
      animations.push({
        bars: [j],
        type: AnimationType.DECOMPARE
      });
    }

    animations.push({
      bars: [compare],
      type: AnimationType.DECOMPARE
    });

    if (min !== i) {
      let tmp = array[i].height;
      animations.push({
        bars: [i],
        height: array[min].height,
        type: AnimationType.RESIZE
      });
      animations.push({
        bars: [min],
        height: tmp,
        type: AnimationType.RESIZE
      });
      array[i].height = array[min].height;
      array[min].height = tmp;
    }
  }
  return array;
};
