import { AnimationType } from "../../constants/enums";
import { cloneArray } from "../../utils";

export function getInsertionSortAnimations(array) {
  const animations = [];
  if (array.length <= 1) return array;

  insertionSort(cloneArray(array), animations);
  return animations;
}

function insertionSort(array, animations) {
  for (var index = 1; index < array.length; index++) {
    var data = array[index].height;
    var dataIndex = index;

    while (dataIndex > 0 && data < array[dataIndex - 1].height) {
      animations.push({
        bars: [dataIndex, dataIndex - 1],
        type: AnimationType.COMPARE
      });
      animations.push({
        bars: [dataIndex],
        height: array[dataIndex - 1].height,
        type: AnimationType.RESIZE
      });

      array[dataIndex].height = array[dataIndex - 1].height;

      animations.push({
        bars: [dataIndex, dataIndex - 1],
        type: AnimationType.DECOMPARE
      });

      dataIndex--;
    }

    animations.push({
      bars: [dataIndex],
      height: data,
      type: AnimationType.RESIZE
    });
    array[dataIndex].height = data;
  }
}
