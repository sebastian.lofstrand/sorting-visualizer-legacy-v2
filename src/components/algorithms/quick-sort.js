import { AnimationType } from "../../constants/enums";
import { cloneArray } from "../../utils";

export function getQuickSortAnimations(array) {
  const animations = [];
  if (array.length <= 1) return array;

  quickSort(cloneArray(array), 0, array.length - 1, animations);
  return animations;
}

function quickSort(array, left, right, animations) {
  let index;

  if (array.length > 1) {
    index = partition(array, left, right, animations);

    if (left < index - 1) {
      quickSort(array, left, index - 1, animations);
    }

    if (index < right) {
      quickSort(array, index, right, animations);
    }
  }

  return array;
}

function partition(array, left, right, animations) {
  let pivotIndex = Math.floor((left + right) / 2);
  let pivot = array[pivotIndex].height;
  let i = left;
  let j = right;

  animations.push({
    bars: [pivotIndex],
    type: AnimationType.COMPARE
  });
  while (i <= j) {
    animations.push({
      bars: [i],
      type: AnimationType.COMPARE
    });
    while (array[i].height < pivot) {
      animations.push({
        bars: [i],
        type: AnimationType.DECOMPARE
      });
      i++;
      animations.push({
        bars: [i],
        type: AnimationType.COMPARE
      });
    }
    if (i !== pivotIndex) {
      animations.push({
        bars: [i],
        type: AnimationType.DECOMPARE
      });
    }

    animations.push({
      bars: [j],
      type: AnimationType.COMPARE
    });
    while (array[j].height > pivot) {
      animations.push({
        bars: [j],
        type: AnimationType.DECOMPARE
      });
      j--;
      animations.push({
        bars: [j],
        type: AnimationType.COMPARE
      });
    }
    if (j !== pivotIndex) {
      animations.push({
        bars: [j],
        type: AnimationType.DECOMPARE
      });
    }

    if (i <= j) {
      animations.push({
        bars: [i],
        height: array[j].height,
        type: AnimationType.RESIZE
      });
      animations.push({
        bars: [j],
        height: array[i].height,
        type: AnimationType.RESIZE
      });
      swap(array, i, j);
      i++;
      j--;
    }
  }

  animations.push({
    bars: [pivotIndex],
    type: AnimationType.DECOMPARE
  });

  return i;
}

function swap(array, a, b) {
  let tmp = array[a].height;
  array[a].height = array[b].height;
  array[b].height = tmp;
}
