import { AnimationType } from "../../constants/enums";
import { cloneArray } from "../../utils";

export function getMergeSortAnimations(array) {
  const animations = [];
  if (array.length <= 1) return array;

  mergeSort(
    cloneArray(array),
    0,
    array.length - 1,
    cloneArray(array),
    animations
  );

  return animations;
}

function mergeSort(array, startIdx, endIdx, helpArray, animations) {
  if (startIdx === endIdx) return;
  const middleIdx = Math.floor((startIdx + endIdx) / 2);
  mergeSort(helpArray, startIdx, middleIdx, array, animations);
  mergeSort(helpArray, middleIdx + 1, endIdx, array, animations);
  merge(array, startIdx, middleIdx, endIdx, helpArray, animations);
}

function merge(array, startIdx, middleIdx, endIdx, helpArray, animations) {
  let k = startIdx;
  let i = startIdx;
  let j = middleIdx + 1;

  while (i <= middleIdx && j <= endIdx) {
    animations.push({
      bars: [i, j],
      type: AnimationType.COMPARE
    });
    animations.push({
      bars: [i, j],
      type: AnimationType.DECOMPARE
    });
    if (helpArray[i].height <= helpArray[j].height) {
      animations.push({
        bars: [k],
        height: helpArray[i].height,
        type: AnimationType.RESIZE
      });
      array[k++] = helpArray[i++];
    } else {
      animations.push({
        bars: [k],
        height: helpArray[j].height,
        type: AnimationType.RESIZE
      });
      array[k++] = helpArray[j++];
    }
  }
  while (i <= middleIdx) {
    animations.push({
      bars: [i],
      type: AnimationType.COMPARE
    });
    animations.push({
      bars: [i],
      type: AnimationType.DECOMPARE
    });
    animations.push({
      bars: [k],
      height: helpArray[i].height,
      type: AnimationType.RESIZE
    });
    array[k++] = helpArray[i++];
  }
  while (j <= endIdx) {
    animations.push({
      bars: [i, j],
      type: AnimationType.COMPARE
    });
    animations.push({
      bars: [i, j],
      type: AnimationType.DECOMPARE
    });
    animations.push({
      bars: [k],
      height: helpArray[j].height,
      type: AnimationType.RESIZE
    });
    array[k++] = helpArray[j++];
  }
}
