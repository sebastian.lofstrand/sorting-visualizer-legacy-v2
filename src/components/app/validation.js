import * as REGEX from "../../constants/regex";
import * as SORTING from "../../constants/sorting";

// Define your state schema
const stateSchema = {
  algorithm: { value: SORTING.MERGE, error: "" },
  animationSpeed: { value: 80, error: "" },
  arraySize: { value: 30, error: "" }
};

// Define your validationStateSchema
// Note: validationStateSchema and stateSchema property
// should be the same in-order validation works!
const validationStateSchema = {
  algorithm: {
    required: true,
    validator: {
      regEx: REGEX.STRING_WITH_DASH,
      error: "Invalid input format."
    }
  },
  animationSpeed: {
    required: true,
    validator: {
      regEx: REGEX.NUM_RANGE_1_100,
      error: "Invalid input format."
    }
  },
  arraySize: {
    required: true,
    validator: {
      regEx: REGEX.NUM_RANGE_1_12,
      error: "Invalid input format."
    }
  }
};

export { stateSchema, validationStateSchema };
