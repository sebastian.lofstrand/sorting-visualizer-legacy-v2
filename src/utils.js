import * as COLORS from "./constants/colors";

const getRandomInt = function(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const newArray = size => {
  let array = [];
  const element = document.getElementById("bars");
  const dimensions = {
    height: element.offsetHeight,
    width: element.offsetWidth
  };

  const barCount = (dimensions.width / 5) * (size / 100);
  for (let id = 0; id < barCount; id++) {
    let height = (id + 1) * (dimensions.height / barCount - 1);
    const element = {
      id,
      height,
      color: COLORS.PRIMARY
    };

    array[id] = element;
  }

  return shuffle(array);
};

const shuffle = array => {
  const shuffleCount = array.length * 5;

  for (let i = 0; i < shuffleCount; i++) {
    let indexA = getRandomInt(0, array.length - 1);
    let indexB = getRandomInt(0, array.length - 1);
    swap(array, indexA, indexB);
  }

  return array;
};

const swap = (array, a, b) => {
  let tmp = array[a];
  array[a] = array[b];
  array[b] = tmp;
};

const cloneArray = array => {
  return JSON.parse(JSON.stringify(array));
};

function isSorted(array) {
  if (array.length === 0) return true;
  for (var i = 1; i < array.length; i++) {
    if (array[i].height < array[i - 1].height) return false;
  }
  return true;
}

export { getRandomInt, newArray, cloneArray, isSorted };
