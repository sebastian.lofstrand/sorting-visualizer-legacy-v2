export const NUM_RANGE_1_12 = /[1-9]|10|11|12/;
export const NUM_RANGE_1_100 = /([1-9]|[1-8][0-9]|9[0-9]|100)/;
export const STRING_WITH_DASH = /([A-Za-z0-9\-_]+)/;
